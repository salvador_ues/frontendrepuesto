import { NgModule } from '@angular/core';
import { Cart } from './cart.models';
import {OrderRepository} from './order.respository';
import {Order} from './order.models';

@NgModule({
    providers: [Cart,Order,OrderRepository]
  })
  export class ModelModule { }