import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {Order} from './order.models';
import {OrderRepository} from './order.respository';
import { from } from 'rxjs';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent {
  orderEnviar=false;
  submitted=false;
 
  constructor(public orederRepository:OrderRepository,public order:Order) { }

  enviarOrden(form:NgForm){
    this.submitted=true;
    if(form.valid){
      this.orederRepository.crearOrden(this.order).subscribe(order=>{
        this.order.clear();
        this.orderEnviar=true; 
        this.submitted=false;
      });
    }
  }
}
