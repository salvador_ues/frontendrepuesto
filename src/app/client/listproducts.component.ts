import { Component, OnInit } from '@angular/core';
import {Productos} from './productocliente';
import {ClientService} from './client.service';
import {ProductosFiltros} from './productocliente';
import {Router, ActivatedRoute} from '@angular/router';
import {Cart} from './cart.models';
import { Observable, from } from 'rxjs';

@Component({
  selector: 'app-listproducts',
  templateUrl: './listproducts.component.html',
  styleUrls: ['./listproducts.component.css']
})
export class ListproductsComponent implements OnInit {
  productos:Productos[];
  categoria:string[];
  sucursales:string[];
  filterPost='';
  // productPerPage=4;
  // selectedPage=1;
  productosFiltros:ProductosFiltros = new ProductosFiltros();
 
  constructor( private cart:Cart,private clienteService:ClientService,private router: Router,private activateRoute:ActivatedRoute) { }

  ngOnInit() {
    this.cart;
    this.loadProducts();
    this.clienteService.getListCategorias().subscribe(
      categoria=>this.categoria=categoria
    );
  }
 
  loadProducts():void{
    this.activateRoute.params.subscribe(params=>{
      let id = params['id']
      console.log("ID categoria:"+id);
     // const pageIndex = (this.selectedPage - 1) * this.productPerPage;
        this.clienteService.getListProducts(id).subscribe(
          productos=>this.productos=productos
        
        );
    });
  }

  public enviarFiltro():void{
    this.clienteService.enviarFiltro(this.productosFiltros).subscribe(
      response=> this.router.navigate(['/client/list_products'])
    );
    console.log(this.productosFiltros);
  }

  public addProductoCaro(producto:Productos){
    this.cart.addLinea(producto);
    this.router.navigateByUrl("/cart");
  }

  

}
