import {Injectable} from '@angular/core';
import {Productos} from './productocliente';

@Injectable()
export class Cart{
    public lineas: CartLine[] = [];
    public cantidadItem:number=0;
    public precioCart: number=0;

    addLinea(producto:Productos,cantidad:number=1){
        let linea = this.lineas.find(linea=>linea.producto.producto_id===producto.producto_id);
        if(linea!== undefined){
            linea.cantidad+=Number(cantidad);
        }else{
            this.lineas.push(new CartLine(producto,cantidad));
        }

        this.recalculate();
    }

    public actualizarCantidad(producto:Productos,cantidad:number){
        let linea= this.lineas.find(p=>p.producto.producto_id===producto.producto_id);
        if(linea!== undefined){
            linea.cantidad = Number(cantidad);
        }

        this.recalculate();
    }

    public removeLine(id:number){
        let index = this.lineas.findIndex(linea=>linea.producto.producto_id===id);
        this.lineas.splice(index,1);
        this.recalculate();
    }

    public clear(){
        this.lineas=[];
        this.cantidadItem=0;
        this.precioCart=0;
    }

    private recalculate(){
        this.cantidadItem=0;
        this.precioCart=0;
        this.lineas.forEach(linea=>{
            this.cantidadItem+=linea.cantidad;
            this.precioCart += (linea.cantidad*linea.producto.precio);

        });
    }
}

export class CartLine{

    constructor(public producto:Productos, public cantidad: number){}

    get lineaTotal(){
        return this.cantidad*this.producto.precio;
    }
}
