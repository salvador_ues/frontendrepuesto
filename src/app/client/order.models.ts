import {Injectable} from '@angular/core';
import {Cart} from './cart.models';

@Injectable()
export class Order{
    public orden_id:number;
    public nombre_remitente:string;
    public direccion:string;
    public ciudad:string;
    public enviado: boolean=false;

    constructor(public cart:Cart){}

    clear(){
        this.orden_id=null;
        this.nombre_remitente=this.direccion=this.ciudad=null;
        this.enviado=false;
        this.cart.clear();
    }
}