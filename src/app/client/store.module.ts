import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { ModelModule } from './model.module';
//import {CounterDirective} from './counter.directive';

@NgModule({
    imports: [ModelModule, BrowserModule, FormsModule],
   // declarations: [CounterDirective],
    // exports: [StoreComponent, CartDetailComponent, CheckoutComponent]
  })
  export class StoreModule {
  
  }