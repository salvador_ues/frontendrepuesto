import { Injectable } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {Productos} from './productocliente';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ProductosFiltros} from './productocliente';
import {Order} from './order.models';
import {Orden} from '../administrator/tablas';
import { Observable, from, throwError } from 'rxjs';
import {Router} from '@angular/router';
import {map,catchError,tap} from 'rxjs/operators';
import Swal from 'sweetalert2';
import {AuthService} from '../login/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private url:string="http://localhost:8000/api_cliente/list_productos";
  private url2:string="http://localhost:8000/api_cliente/list_subcategorias";
  private url3:string="http://localhost:8000/api_cliente/list_sucursales";
  private url4:string="http://localhost:8000/api_cliente/list_productos_filtros";
  private url5:string ="http://localhost:8000/api_cliente/order";
  private url6:string ="http://localhost:8000/api_cliente/list_ordenes/page/";

  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient,private router:Router,private authService:AuthService) { }

  private agregarAuthorizationHeader(){
    let token = this.authService.token;
    if(token!=null){
      return this.httpHeaders.append('Authorization','Bearer '+token);
    }
    return this.httpHeaders;
  }

  private noAutorizado(e):boolean{
    var ruta='';
    if(e.status==401){
      if(this.authService.esAutenticado()){
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    } 
    if(e.status==403){
      if(this.authService.usuario.roles.includes('ROLE_ADMIN')){
        ruta = '/administrador';
      }
      if(this.authService.usuario.roles.includes('ROLE_NEGOCIO')){
        ruta = '/bussines';
      }
      if(this.authService.usuario.roles.includes('ROLE_USER')){
        ruta = '/client';
      }
      Swal.fire('Acceso denegado','No tiene acceso','warning');
      this.router.navigate([ruta]);
      return true;
    }
    return false;
  }

  getListOrdenes(page:number):Observable<any>{
    return this.http.get(this.url6+page,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      }),
      map((response:any)=>{(response.content as Orden[]).map(order=>{
        return order;
      }); 
      return response;
      })
    )
  }

  getListProducts(id):Observable<Productos[]>{
    return this.http.get<Productos[]>(`${this.url}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      })
    );
  }

  getListCategorias():Observable<string[]>{
    return this.http.get<string[]>(this.url2,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      })
    );
  }

  // getListSucursales():Observable<string[]>{
  //   return this.http.get<string[]>(this.url3,{headers:this.agregarAuthorizationHeader()}).pipe(
  //     catchError(e=>{
  //       if(this.noAutorizado(e)){
  //         return throwError(e);
  //       }
  //       if(e.status==400){
  //         return throwError(e);
  //       }
  //       console.log(e.error.respuesta);
  //       return throwError(e);
  //     })
  //   );
  // }

  enviarFiltro(productosFiltros:ProductosFiltros):Observable<ProductosFiltros>{
    return this.http.post<ProductosFiltros>(this.url4,productosFiltros,{headers:this.agregarAuthorizationHeader()})
  }

  // guardarOrden(order:Order):Observable<Order>{
  //   console.log(JSON.stringify(order));
  //   console.log(order);
  //   //return from([order]);
  //   return this.http.post<Order>(this.url5,order,{headers:this.agregarAuthorizationHeader()}).pipe(
  //     catchError(e=>{
  //       if(this.noAutorizado(e)){
  //         return throwError(e); 
  //       }
  //       if(e.status==400){
  //         return throwError(e);
  //       }
  //       console.log(e.error.respuesta);
  //       return throwError(e);
  //     })
  //   );
  // }
 
  crearOrden(order:Order):Observable<Order>{
    return this.http.post<Order>(this.url5,order,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        //console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al crear orden',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

 
}
