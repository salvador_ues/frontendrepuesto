import { Component, OnInit } from "@angular/core";
import {ClientService} from './client.service';
import {Order} from './order.models';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-ordenesclient",
  templateUrl: "./ordenesclient.component.html"
})
export class OrdenesclientComponent implements OnInit {
  public order:Order[];
  paginador: any;
  constructor(private clienteService:ClientService,private router:Router,private activateRoute:ActivatedRoute) {}

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params=>{
      let page: number = +params.get("page");
      if (!page) {
        page = 0;
      }
      this.clienteService.getListOrdenes(page).subscribe(response=>{
        this.order = response.content as Order[];
        this.paginador = response;
      })
    })
  }
}
