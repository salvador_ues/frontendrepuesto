export class Productos{
    producto_id:number;
    nombre:string;
    descripcion:string;
    cantidad:any;
    precio:any;
    subcategoria:string;
}

export class ProductosFiltros{
    nombreCategoria:string;
    nombreSucursal:string;
}
