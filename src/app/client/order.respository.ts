import {Injectable} from '@angular/core';
import { Observable, from } from 'rxjs';
import {Order} from './order.models';
import {ClientService} from './client.service';

@Injectable()
export class OrderRepository{
    private orden:Order[]=[];

    constructor(private clienService:ClientService){}

    getOrden():Order[]{
        return this.orden;
    } 

    // guardarOrden(orden:Order):Observable<Order>{
    //     return this.clienService.guardarOrden(orden);
    // }

    crearOrden(orden:Order):Observable<Order>{
        return this.clienService.crearOrden(orden);
    }

}