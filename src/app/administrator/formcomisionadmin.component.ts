import { Component, OnInit } from '@angular/core';
import {Comision} from './tablas';
import {AdministratorService} from './administrator.service';
import {Router,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-formcomisionadmin',
  templateUrl: './formcomisionadmin.component.html',
  styleUrls: ['./formcomisionadmin.component.css']
})
export class FormcomisionadminComponent implements OnInit {
  private comision:Comision = new Comision();
  estado: any=[
    {id:true,name:"Activo"},
    {id:false,name:"Inactivo"},
  ];
  constructor(private adminService:AdministratorService,private router: Router,private activatedRoute:ActivatedRoute) { }
   

  ngOnInit() {
    this.getComision();
  }

  public getComision():void{
    this.activatedRoute.params.subscribe(params=>{
      let id=params['id'];
      if(id){
        this.adminService.getComision(id).subscribe(
          (com)=>this.comision=com
        )
      }
    })
  }

  comisionCrear():void{
    this.adminService.comisionCrear(this.comision).subscribe(
      response=>{this.router.navigate(['/administrador/list_comisiones'])
      Swal.fire('Nuevo registro','Registro creado con exito','success') 
    }
    )
  }

  comisionModificar():void{
    this.adminService.comisionModificar(this.comision).subscribe(
      response=>{this.router.navigate(['/administrador/list_comisiones'])
      Swal.fire('Registro editado','Registro editado con exito','success') 
    }
    );
  }



}
