import { Injectable } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, from, throwError } from 'rxjs'
import {Comision,Sucursal,Administradores,UsuarioAdmin} from './tablas';
import {map,catchError,tap} from 'rxjs/operators';
import {Orden} from './tablas';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {AuthService} from '../login/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdministratorService {
  private url:string = "http://localhost:8000/api_admin/comisiones";
  private url2:string ="http://localhost:8000/api_admin/comision";
  private url3:string ="http://localhost:8000/api_admin/sucursales";
  private url4:string = "http://localhost:8000/api_admin/sucursal";
  private url5:string ="http://localhost:8000/api_admin/administradores_sucursales";
  private url6:string = "http://localhost:8000/api_admin/administrador";
  private url7:string = "http://localhost:8000/api_admin/list_ordenes/page/";
  //private url8:string = "http://localhost:8081/api_admin/orden/";

  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient,private router:Router,private authService:AuthService) { }

  private agregarAuthorizationHeader(){
    let token = this.authService.token;
    if(token!=null){
      return this.httpHeaders.append('Authorization','Bearer '+token);
    }
    return this.httpHeaders;
  }

  private noAutorizado(e):boolean{
    if(e.status==401){
      if(this.authService.esAutenticado()){
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if(e.status==403){
      var ruta='';
      if(this.authService.usuario.roles.includes('ROLE_ADMIN')){
        ruta = '/administrador';
      }
      if(this.authService.usuario.roles.includes('ROLE_NEGOCIO')){
        ruta = '/bussines';
      }
      if(this.authService.usuario.roles.includes('ROLE_USER')){
        ruta = '/client';
      }
      Swal.fire('Acceso denegado','No tiene acceso','warning');
      this.router.navigate([ruta]);
    }
    return false;
  }

  getAdmin(id):Observable<UsuarioAdmin>{
    return this.http.get<UsuarioAdmin>(`${this.url6}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        this.router.navigate(['/bussines/list_admin_sucursales']);
        Swal.fire({
          icon:'error',
          title:'Error al editar administrador',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  adminCrear(admin:UsuarioAdmin):Observable<UsuarioAdmin>{
    return this.http.post<UsuarioAdmin>(this.url6,admin,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al crear administrador',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  adminModificar(admin:UsuarioAdmin):Observable<UsuarioAdmin>{
    return this.http.put<UsuarioAdmin>(`${this.url6}/${admin.administrador.administrador_id}`,admin,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al actualizar administrador',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  adminBloquear(id:number):Observable<Administradores>{
    return this.http.delete<Administradores>(`${this.url6}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al bloquear administrador',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  getListAdministradores2():Observable<Administradores[]>{
      return this.http.get<Administradores[]>(this.url5,{headers:this.agregarAuthorizationHeader()}).pipe(
        catchError(e=>{
          if(this.noAutorizado(e)){
            return throwError(e);
          }
          if(e.status==400){
            return throwError(e);
          }
          console.log(e.error.respuesta);
          return throwError(e);
        })
      );
  }

  getListAdministradores(page:number):Observable<any>{
    return this.http.get(this.url5+'/page/'+page,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      }),
      map((response:any)=>{
        (response.content as Administradores[]).map(admin=>{
          admin.nombreNegocio = admin.nombreNegocio.toUpperCase();
          return admin;
        })
        return response;
      })
    );
    //return this.http.get<Administradores[]>(this.url5);
  }

  getListComisiones(page:number):Observable<any>{
    return this.http.get(this.url + '/page/'+page,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      }),
      map((response:any)=>{
        return response;
      })
    );
    //return this.http.get<Comision[]>(this.url);
  }

  getComision(id):Observable<Comision>{
    return  this.http.get<Comision>(`${this.url2}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        this.router.navigate(['/bussines/list_comisiones']);
        Swal.fire({
          icon:'error',
          title:'Error al editar comision',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  comisionCrear(comision:Comision):Observable<Comision>{
    return this.http.post<Comision>(this.url2,comision,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al crear comisión',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  comisionModificar(comision:Comision):Observable<Comision>{
    return this.http.put<Comision>(`${this.url2}/${comision.comision_id}`,comision,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al actualizar comisión',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  comisionBloquear(id:number):Observable<Comision>{
    return this.http.delete<Comision>(`${this.url2}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al eliminar comisión',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  getListSucursales(page:number):Observable<any>{
    return this.http.get(this.url3+'/page/'+page,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      }),
      map((response:any)=>{
        return response;
      })
    );
    //return this.http.get<Sucursal[]>(this.url3);
  }

  getSucursal(id):Observable<Sucursal>{
    return this.http.get<Sucursal>(`${this.url4}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        this.router.navigate(['/administrador/list_negocios']);
        Swal.fire({
          icon:'error',
          title:'Error al editar sucursal',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  sucursalCrear(sucursal:Sucursal):Observable<Sucursal>{
    return this.http.post<Sucursal>(this.url4,sucursal,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al crear sucursal',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  sucursalModificar(sucursal:Sucursal):Observable<Sucursal>{
    return this.http.put<Sucursal>(`${this.url4}/${sucursal.sucursal_id}`,sucursal,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al actualizar sucursal',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  sucursalEliminar(id:number):Observable<Sucursal>{
    return this.http.delete<Sucursal>(`${this.url4}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al bloquear sucursal',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  getListOrdenes(page:number):Observable<any>{
    return this.http.get(this.url7+page,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      }),
      map((response:any)=>{
        return response;
      }),
    );
  }

  // getOrden(id):Observable<Orden>{
  //   return this.http.get<Orden>(`${this.url8}/${id}`);
  // }
 
}
