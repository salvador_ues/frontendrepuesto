import { Component, OnInit } from '@angular/core';
import {AdministratorService} from './administrator.service';
import {Router,ActivatedRoute} from '@angular/router';
import {Sucursal,Comision,UsuarioAdmin,Administradores} from './tablas';
import {Usuario} from '../login/usuario';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-formadminsucursal',
  templateUrl: './formadminsucursal.component.html',
  styleUrls: ['./formadminsucursal.component.css']
})
export class FormadminsucursalComponent implements OnInit {
  private administrador:Administradores=new Administradores();
  private usuario:Usuario = new Usuario();
  private password2:string;
  private usuarioAdmin:UsuarioAdmin = new UsuarioAdmin();
  //private sucursal:Sucursal = new Sucursal();

  constructor(private administratorService:AdministratorService,private router: Router,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.getAdmin();
  } 

  public getAdmin():void{
    this.activatedRoute.params.subscribe(params=>{
      let id=params['id'];
      if(id){
        this.administratorService.getAdmin(id).subscribe(
          (admin)=>{this.usuarioAdmin=admin,this.usuario = this.usuarioAdmin.usuario,this.administrador = this.usuarioAdmin.administrador}
        )
      }
    })
  }

  public administradorCrear():void{
    // this.administratorService.adminCrear(this.administrador).subscribe(
    //   response=>this.router.navigate(['/administrador/list_admin_sucursales'])
    // );
    if(this.usuario.password.localeCompare(this.password2)!=0){
      Swal.fire('Error password','Claves no coinciden','error');
    }else{
      this.usuarioAdmin.usuario = this.usuario;
      this.usuarioAdmin.administrador = this.administrador;
      this.administratorService.adminCrear(this.usuarioAdmin).subscribe(
        response=>{this.router.navigate(['/administrador/list_admin_sucursales'])
                  Swal.fire('Nuevo registro','Registro creado con exito','success')              
      }
      );
      
      console.log(this.usuarioAdmin.usuario);
      console.log(this.usuarioAdmin.administrador);
    }
    
  }

  public administradorModificar():void{
    this.usuarioAdmin.usuario = this.usuario;
    this.usuarioAdmin.administrador = this.administrador;
    this.administratorService.adminModificar(this.usuarioAdmin).subscribe(
      response=>{this.router.navigate(['/administrador/list_admin_sucursales'])
      Swal.fire('Registro editado','Registro editado con exito','success') 
    }
    );
  }

}
