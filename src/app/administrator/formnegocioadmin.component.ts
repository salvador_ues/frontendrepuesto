import { Component, OnInit } from '@angular/core';
import {AdministratorService} from './administrator.service';
import {Router,ActivatedRoute} from '@angular/router';
import {Sucursal,Comision,Administradores} from './tablas';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-formnegocioadmin',
  templateUrl: './formnegocioadmin.component.html',
  styleUrls: ['./formnegocioadmin.component.css']
})
export class FormnegocioadminComponent implements OnInit {
  administradores:Administradores[];
  private sucursal:Sucursal = new Sucursal();
  // estado: any=[
  //   {id:'activo',nombre:"Activo"},
  //   {id:'inactivo',nombre:"Inactivo"},
  // ]; 
  constructor(private administratorService:AdministratorService,private router: Router,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    
    this.administratorService.getListAdministradores2().subscribe(
      administradores=>(this.administradores=administradores)
    );
    this.getSucursal();
  }

  public getSucursal():void{
    this.activatedRoute.params.subscribe(params=>{
      let id=params['id'];
      console.log("id sucursal:"+id);
      if(id){
        this.administratorService.getSucursal(id).subscribe(
          (sucursal)=>this.sucursal=sucursal
        )
      }
    })
    console.log(this.sucursal);
  }

  public sucursalCrear():void{
    this.administratorService.sucursalCrear(this.sucursal).subscribe(
      response=>{this.router.navigate(['/administrador/list_negocios'])
      Swal.fire('Nuevo registro','Registro creado con exito','success') 
    }
    )
    console.log(this.sucursal)
  }

  public sucursalModificar():void{
    this.administratorService.sucursalModificar(this.sucursal).subscribe(
      response=>{this.router.navigate(['/administrador/list_negocios'])
      Swal.fire('Registro editado','Registro editado con exito','success') 
    }
    )
  }

  compararAdmin(obj1:Administradores,obj2:Administradores){
    if(obj1 === undefined && obj2 === undefined){
      return true;
   }
  return obj1===null||obj2===null || obj1===undefined|| obj2===undefined ? false : obj1.administrador_id === obj2.administrador_id;
  }

}
