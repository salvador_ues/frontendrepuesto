import {Usuario} from '../login/usuario';

export class Comision{
    comision_id:number;
    comision:any;
    estado:boolean;
}

export class Sucursal{
    sucursal_id:number;
    nombre:string;
    email:string;
    administrador:Administradores;
}

export class Administradores{
    administrador_id:number;
    nombreNegocio:string;
    usuario:Usuario;
    estado:boolean;
}

export class Orden{
    orden_id:number;
    nombre_remitente:string;
    direccion:string;
    ciudad:string;
    enviado:boolean;
    cart:Cart;
}

export class Cart{
    carro_id:number;
    cantidadItem:number;
    precioCart:any;
    comision:any;
    montoComision:any;
}

export class UsuarioAdmin{
    usuario:Usuario;
    administrador:Administradores;

    public get _usuario() : Usuario {
        return this.usuario;
    }

    public set _usuario(usuario:Usuario){
        this.usuario;
    }

    public set _administrador(administrador : Administradores) {
        this.administrador;
    }

    public get _administrador():Administradores{
        return this.administrador;
    }
}
