import { Component, OnInit } from "@angular/core";
import { Sucursal, Comision, Administradores } from "./tablas";
import { AdministratorService } from "./administrator.service";
import swal from "sweetalert2";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";

@Component({
  selector: "app-listadminsucursal",
  templateUrl: "./listadminsucursal.component.html",
  styleUrls: ["./listadminsucursal.component.css"]
})
export class ListadminsucursalComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  // dtTrigger:Subject = new Subject();
  administradores: Administradores[];
  paginador: any;
  constructor(
    private administratorService: AdministratorService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params => {
      let page: number = +params.get("page");
      if (!page) {
        page = 0;
      }
      this.administratorService
        .getListAdministradores(page)
        .subscribe(response=>{
          this.administradores = response.content as Administradores[];
          this.paginador = response;
        }
        );
    });

    this.dtOptions = {
      pagingType: "full_numbers"
      // pageLength:4,
    };
  }

  administradorBloquear(administrador: Administradores): void {
    swal
      .fire({
        title: "Estás seguro?",
        text:
          "Una ves bloqueado se podra deshacer la operacion desde otro formulario!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, bloquea este registro!"
      })
      .then(result => {
        if (result.value) {
          this.administratorService
            .adminBloquear(administrador.administrador_id)
            .subscribe(response => {
              this.administradores = this.administradores.filter(
                admin => admin !== administrador
              );
              swal.fire(
                "Bloqueado!",
                "El Administrador ha sido bloqueado.",
                "success"
              );
            });
        }
      });
  }
}
