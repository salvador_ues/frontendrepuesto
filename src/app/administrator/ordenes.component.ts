import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import swal from "sweetalert2";
import {Orden} from './tablas';
import {Order} from '../client/order.models';
import { AdministratorService } from "./administrator.service";

@Component({
  selector: 'app-ordenes',
  templateUrl: './ordenes.component.html',
  styleUrls: ['./ordenes.component.css']
})
export class OrdenesComponent implements OnInit {
  public ordenes:Orden[];
  paginador: any;

  constructor(private administratorService: AdministratorService, private router: Router,private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params=>{
      let page: number = +params.get("page");
      if (!page) {
        page = 0; 
      } 
      this.administratorService.getListOrdenes(page).subscribe(
        response=>{
          this.ordenes = response.content as Orden[];
          this.paginador = response;
        }
      );
    })
  }

}
