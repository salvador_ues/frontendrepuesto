import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import swal from "sweetalert2";
import { AdministratorService } from "./administrator.service";
import { Sucursal, Comision } from "./tablas";

@Component({
  selector: "app-administrator",
  templateUrl: "./administrator.component.html",
  styleUrls: ["./administrator.component.css"]
})
export class AdministratorComponent implements OnInit {
  sucursal: Sucursal[];
  paginador: any;
  constructor(
    private administratorService: AdministratorService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params => {
      let page: number = +params.get("page");
      if (!page) {
        page = 0;
      }
      this.administratorService
        .getListSucursales(page)
        .subscribe(response=>{
          this.sucursal = response.content as Sucursal[];
          this.paginador = response;
        });
    });
  }

  sucursalEliminar(sucursal: Sucursal): void {
    swal
      .fire({
        title: "Estás seguro?",
        text:
          "Una ves bloqueado se podra deshacer la operacion desde otro formulario!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, bloquea este registro!"
      })
      .then(result => {
        if (result.value) {
          this.administratorService
            .sucursalEliminar(sucursal.sucursal_id)
            .subscribe(response => {
              this.sucursal = this.sucursal.filter(suc => suc !== sucursal);
              swal.fire(
                "Bloqueado!",
                "La sucursal negocio ha sido bloqueado.",
                "success"
              );
            });
        }
      });
  }
}
