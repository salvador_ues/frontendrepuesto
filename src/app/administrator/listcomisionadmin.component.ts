import { Component, OnInit } from '@angular/core';
import swal from "sweetalert2";
import {Comision } from "./tablas";
import { Router, ActivatedRoute } from "@angular/router";
import { AdministratorService } from "./administrator.service";

@Component({
  selector: 'app-listcomisionadmin',
  templateUrl: './listcomisionadmin.component.html',
  styleUrls: ['./listcomisionadmin.component.css']
})
export class ListcomisionadminComponent implements OnInit {
  public comisiones:Comision[];
  paginador: any;

  constructor(private administratorService: AdministratorService, private router: Router,private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params=>{
      let page: number = +params.get("page");
      if (!page) {
        page = 0; 
      } 
      this.administratorService.getListComisiones(page).subscribe(
        response=>{
          this.comisiones = response.content as Comision[];
          this.paginador = response;
        }
      );
    })

  }

  comisionBloquear(comision:Comision):void{
    swal
    .fire({
      title: "Estás seguro?",
      text:
        "Una ves bloqueado se podra deshacer la operacion desde otro formulario!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, bloquea este registro!"
    })
    .then(result => {
      if (result.value) {
        this.administratorService
          .adminBloquear(comision.comision_id)
          .subscribe(response => {
            this.comisiones = this.comisiones.filter(
              com => com !== comision
            );
            swal.fire(
              "Bloqueado!",
              "La comisión ha sido bloqueada.",
              "success"
            );
          });
      }
    });
  }
}
