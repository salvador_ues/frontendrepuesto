import { Component, OnInit } from '@angular/core';
import {BussinesService} from './bussines.service';
import swal from "sweetalert2";
import {Productos,SubCategorias} from './producto';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-listcategorias',
  templateUrl: './listcategorias.component.html',
  styleUrls: ['./listcategorias.component.css']
})
export class ListcategoriasComponent implements OnInit {
  categorias:SubCategorias[];
  paginador: any;
  constructor(private bussinesService:BussinesService,private router: Router,private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params=>{
      let page: number = +params.get("page");
      if (!page) {
        page = 0;
      }
      this.bussinesService.getListCategorias(page).subscribe(response=>{
        this.categorias = response.content as SubCategorias[];
        this.paginador = response;
      }
    );
    }
    ); 
  }

  categoriaBloquear(categoria:SubCategorias):void{
    swal
      .fire({
        title: "Estás seguro?",
        text: "Una ves eliminado no se podra deshacer la operacion!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, elimina este registrot!"
      }) 
      .then(result => {
        if (result.value) {
          this.bussinesService.categoriaBloquear(categoria.subcategoria_id).subscribe(response => {
            this.categorias=this.categorias.filter(cat=>cat!==categoria)
            swal.fire(
              "Eliminado!",
              "Tu registro ha sido eliminado.",
              "success"
            );
          });
        }
      });
  }
}
