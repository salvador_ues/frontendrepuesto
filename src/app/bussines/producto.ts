export class Productos{
    producto_id:number;
    nombre:string;
    descripcion:string;
    precio:any;
    cantidad:any;
    subcategoria:SubCategorias;
    estado:boolean;
}

export class SubCategorias{
    subcategoria_id:number;
    nombre:string;
    estado:boolean;
}

export class CartLine{
    linea_carro_id:number;
    producto: Productos;
    cantidad:number;
    lineaTotal:any;
    comision:any;
    comisionMonto:any;
    gananciaNeta:any;
}