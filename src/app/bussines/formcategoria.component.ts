import { Component, OnInit } from '@angular/core';
import {BussinesService} from './bussines.service';
import {SubCategorias,Productos} from './producto';
import {Router,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-formcategoria',
  templateUrl: './formcategoria.component.html',
  styleUrls: ['./formcategoria.component.css']
})
export class FormcategoriaComponent implements OnInit {
  private categoria:SubCategorias = new SubCategorias();
  constructor(private bussinesService:BussinesService,private router: Router,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.getCategoria();
  }

  public getCategoria():void{
    this.activatedRoute.params.subscribe(params=>{
      let id=params['id'];
      if(id){
        this.bussinesService.getCategoria(id).subscribe(
          (categoria)=>this.categoria=categoria
        )
      }
    })
  }

  public categoriaCrear():void{
    this.bussinesService.categoriaCrear(this.categoria).subscribe(
      response=>{this.router.navigate(['/bussines/list_categorias'])
      Swal.fire('Nuevo registro','Registro creado con exito','success') 
    }
    );
  }

  public categoriaModificar():void{
    this.bussinesService.categoriaModificar(this.categoria).subscribe(
      response=> {this.router.navigate(['/bussines/list_categorias'])
      Swal.fire('Registro editado','Registro editado con exito','success') 
    }
    );
  }
 
}
