import { Component, OnInit } from '@angular/core';
import {BussinesService} from './bussines.service';
import {SubCategorias,Productos} from './producto';
import {Router,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-formproductcomponent',
  templateUrl: './formproductcomponent.component.html',
  styleUrls: ['./formproductcomponent.component.css']
})
export class FormproductcomponentComponent implements OnInit {
  subCategorias:SubCategorias[];
  private producto: Productos= new Productos();
  estado: any=[
    {id:true,name:"Activo"},
    {id:false,name:"Inactivo"},
  ];

  constructor(private bussinesService:BussinesService,private router: Router,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.bussinesService.getListCategorias2()
    .subscribe(subCategorias => (this.subCategorias = subCategorias))
    this.getProducto();
  } 

  public getProducto():void{
    this.activatedRoute.params.subscribe(params=>{
      let id=params['id'];
      if(id){
        this.bussinesService.getProducto(id).subscribe(
          (producto)=>this.producto=producto
        )
      }
    })
  }

  public productoModificar():void{
    this.bussinesService.productoModificar(this.producto).subscribe(
      response=> {this.router.navigate(['/bussines/list_products'])
      Swal.fire('Registro editado','Registro editado con exito','success') 
    }
    )
   console.log(this.producto);
  }

  public productoCrear():void{
    this.bussinesService.productoCrear(this.producto).subscribe(
       response=>{this.router.navigate(['/bussines/list_products'])
       Swal.fire('Nuevo registro','Registro creado con exito','success') 
      }
     )
     console.log(this.producto)
 }

 compararSubCategoria(obj1:SubCategorias,obj2:SubCategorias):boolean{
   if(obj1 === undefined && obj2 === undefined){
      return true;
   }
  return obj1===null||obj2===null || obj1===undefined|| obj2===undefined ? false : obj1.subcategoria_id === obj2.subcategoria_id;
}

compararEstado(estado:boolean,estado2:boolean):boolean{
  if(estado === undefined && estado2 === undefined){
     return true;
  }
 return estado===null||estado2===null || estado===undefined|| estado2===undefined ? false : estado === estado2;
}

}
