import { Component, OnInit } from '@angular/core';
import {Productos} from './producto';
import {BussinesService} from './bussines.service';
import swal from "sweetalert2";
import { Router, ActivatedRoute } from "@angular/router";

@Component({ 
  selector: 'app-listproductsbussines',
  templateUrl: './listproductsbussines.component.html',
  styleUrls: ['./listproductsbussines.component.css']
})
export class ListproductsbussinesComponent implements OnInit {
  productos:Productos[];
  paginador:any;
  constructor(private bussinesService:BussinesService,  private router: Router,private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params=>{
      let page: number = +params.get("page");
      if (!page) {
        page = 0;
      } 
      this.bussinesService.getListProducts(page).subscribe(response=>{
        this.productos = response.content as Productos[];
        this.paginador = response;
      }
    );
    });

    
  }

  productoEliminar(producto:Productos):void{
    swal
      .fire({
        title: "Estás seguro?",
        text: "Una ves bloqueado se podra deshacer la operacion!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, bloquea este registrot!"
      })
      .then(result => {
        if (result.value) {
          this.bussinesService.productoEliminar(producto.producto_id).subscribe(response => {
            this.productos=this.productos.filter(pro=>pro!==producto)
            swal.fire(
              "Eliminado!",
              "Tu registro ha sido bloqueado.",
              "success"
            );
          });
        }
      });
  }

}
