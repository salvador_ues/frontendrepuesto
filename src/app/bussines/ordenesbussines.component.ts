import { Component, OnInit } from '@angular/core';
import {CartLine} from './producto';
import {BussinesService} from './bussines.service';

@Component({
  selector: 'app-ordenesbussines',
  templateUrl: './ordenesbussines.component.html',
  styleUrls: ['./ordenesbussines.component.css']
})
export class OrdenesbussinesComponent implements OnInit {
  public cartLine:CartLine[];
  constructor(private bussinesService:BussinesService) { }

  ngOnInit() {
    this.bussinesService.getListCartLine().subscribe(
      params=>this.cartLine=params
    );
  }

}
