import { Injectable } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, from, throwError } from 'rxjs'
import {Productos,SubCategorias,CartLine} from './producto';
import {map,catchError,tap} from 'rxjs/operators';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {AuthService} from '../login/auth.service';

@Injectable({
  providedIn: 'root'
}) 
export class BussinesService {
  private url:string ="http://localhost:8000/api_sucursal/list_products";
  private url2:string ="http://localhost:8000/api_sucursal/mostrar_producto";
  private url3:string ="http://localhost:8000/api_sucursal/crear_producto";
  private url4:string ="http://localhost:8000/api_sucursal/modificar_producto";
  private url5:string ="http://localhost:8000/api_sucursal/eliminar_producto";
  private url6:string ="http://localhost:8000/api_sucursal/list_categorias";
  private url7:string ="http://localhost:8000/api_sucursal/categoria";
  private url8:string ="http://localhost:8000/api_sucursal/linea_carrito";

  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient,private router:Router,private authService:AuthService) { }

  private agregarAuthorizationHeader(){
    let token = this.authService.token;
    if(token!=null){
      return this.httpHeaders.append('Authorization','Bearer '+token);
    }
    return this.httpHeaders;
  }

  private noAutorizado(e):boolean{
    if(e.status==401){
      if(this.authService.esAutenticado()){
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if(e.status==403){
      var ruta='';
      if(this.authService.usuario.roles.includes('ROLE_ADMIN')){
        ruta = '/administrador';
      }
      if(this.authService.usuario.roles.includes('ROLE_NEGOCIO')){
        ruta = '/bussines';
      }
      if(this.authService.usuario.roles.includes('ROLE_USER')){
        ruta = '/client';
      }
      Swal.fire('Acceso denegado','No tiene acceso','warning');
      this.router.navigate([ruta]);
      return true;
    }
    return false;
  }

  getCategoria(id):Observable<SubCategorias>{
    return this.http.get<SubCategorias>(`${this.url7}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        this.router.navigate(['/bussines/list_categorias']);
        Swal.fire({
          icon:'error',
          title:'Error al editar',
          text:e.error.respuesta+' '+'error',
        });
        return throwError(e);
      })
    );
  }

  categoriaCrear(categoria:SubCategorias):Observable<SubCategorias>{
    return this.http.post<SubCategorias>(this.url7,categoria,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al crear categoria',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  categoriaModificar(categoria:SubCategorias):Observable<SubCategorias>{
    return this.http.put<SubCategorias>(`${this.url7}/${categoria.subcategoria_id}`,categoria,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al editar categoria',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  categoriaBloquear(id:number):Observable<SubCategorias>{
    return this.http.delete<SubCategorias>(`${this.url7}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al bloquear categoria',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  getListCategorias2():Observable<SubCategorias[]>{
    return this.http.get<SubCategorias[]>(this.url6,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      })
    );
  }

  getListCategorias(page:number):Observable<any>{
    return this.http.get(this.url6+'/page/'+page,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      }),
      map((response:any)=>{(response.content as SubCategorias[]).map(categoria=>{
        categoria.nombre = categoria.nombre.toUpperCase();
        return categoria;
      }); 
      return response;
      })
    ); 
  }

  getListProducts(page:number):Observable<any>{
    return this.http.get(this.url+'/page/'+page,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      }),
      map((response:any)=>{
        (response.content as Productos[]).map(producto=>{
          producto.nombre = producto.nombre.toUpperCase();
          producto.descripcion = producto.descripcion.toUpperCase();
          return producto;
        });
        return response;
      })
    );
  }

  getProducto(id):Observable<Productos>{
    return this.http.get<Productos>(`${this.url2}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        this.router.navigate(['/bussines/list_products']);
        Swal.fire({
          icon:'error',
          title:'Error al editar producto',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  productoCrear(producto:Productos):Observable<Productos>{
    return this.http.post<Productos>(this.url3,producto,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al crear producto',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }  

  productoModificar(producto:Productos):Observable<Productos>{
    return this.http.put<Productos>(`${this.url4}/${producto.producto_id}`,producto,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al modificar producto',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  productoEliminar(id:number):Observable<Productos>{
    return this.http.delete<Productos>(`${this.url5}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al eliminar producto',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  getListCartLine():Observable<CartLine[]>{
    return this.http.get<CartLine[]>(this.url8,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      })
    );
  }
}
