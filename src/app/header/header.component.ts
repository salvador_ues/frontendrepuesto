import { Component, OnInit } from '@angular/core';
import {AuthService} from '../login/auth.service';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
    this.authService.logout();
  }

  logout():void{
    this.authService.logout();
    Swal.fire('Logout','Has cerrado sesion con exito','info');
    this.router.navigate(['/login']);
  }

}
