import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'ordenescliente-nav',
  templateUrl: './ordenescliente.component.html',
})
export class OrdenesclienteComponent implements OnInit {
  @Input() paginador:any;
  paginas:number[];
  constructor() { }

  ngOnInit() {
    this.paginas = new Array(this.paginador.totalPages).fill(0).map((valor,indice)=>indice+1);
  }

}
