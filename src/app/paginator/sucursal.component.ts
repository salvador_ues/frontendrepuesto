import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'sucursal-nav',
  templateUrl: './sucursal.component.html',
})
export class SucursalComponent implements OnInit {
  @Input() paginador:any;
  paginas:number[];
  constructor() { }

  ngOnInit() {
    this.paginas = new Array(this.paginador.totalPages).fill(0).map((valor,indice)=>indice+1);
  }

}
