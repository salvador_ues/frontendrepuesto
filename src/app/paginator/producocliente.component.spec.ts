import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProducoclienteComponent } from './producocliente.component';

describe('ProducoclienteComponent', () => {
  let component: ProducoclienteComponent;
  let fixture: ComponentFixture<ProducoclienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProducoclienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProducoclienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
