import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'comision-nav',
  templateUrl: './comision.component.html',
})
export class ComisionComponent implements OnInit {
  @Input() paginador:any;
  paginas:number[];
  constructor() { }

  ngOnInit() {
    this.paginas = new Array(this.paginador.totalPages).fill(0).map((valor,indice)=>indice+1);
  }

}
