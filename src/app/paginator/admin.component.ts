import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'admin-nav',
  templateUrl: './admin.component.html',
})
export class AdminComponent implements OnInit {
  @Input() paginador:any;
  paginas:number[];
  constructor() { }

  ngOnInit() {
    this.paginas = new Array(this.paginador.totalPages).fill(0).map((valor,indice)=>indice+1);
  }

}
 