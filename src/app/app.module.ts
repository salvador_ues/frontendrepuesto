import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { HotTableModule } from '@handsontable/angular';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoadDataBaseComponent } from './load-data-base/load-data-base.component';
import {LoaddbService} from './load-data-base/loaddb.service';
import { ClientComponent } from './client/client.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { BussinesComponent } from './bussines/bussines.component';
import { Routes, RouterModule, Router } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { FormconnectionComponent } from './load-data-base/formconnection.component';
import { Select2Module } from 'ng2-select2';
import { SelecolumnComponent } from './load-data-base/selecolumn.component';
import { ListconexionesComponent } from './load-data-base/listconexiones.component';
import { LoadexcelComponent } from './load-data-base/loadexcel.component';
import { ListproductsComponent } from './client/listproducts.component';
import { ListproductsbussinesComponent } from './bussines/listproductsbussines.component';
import { FilterPipe } from './pipes/filter.pipe';
import { RelationcolumnComponent } from './load-data-base/relationcolumn.component';
import { FormproductcomponentComponent } from './bussines/formproductcomponent.component';
import { FormnegocioadminComponent } from './administrator/formnegocioadmin.component';
import { FormcomisionadminComponent } from './administrator/formcomisionadmin.component';
import { ListcomisionadminComponent } from './administrator/listcomisionadmin.component';
import { ListcategoriasComponent } from './bussines/listcategorias.component';
import { FormcategoriaComponent } from './bussines/formcategoria.component';
import { FormadminsucursalComponent } from './administrator/formadminsucursal.component';
import { ListadminsucursalComponent } from './administrator/listadminsucursal.component';
import {registerLocaleData} from '@angular/common';
import localeES from '@angular/common/locales/es';
import { PaginatorComponent } from './paginator/paginator.component';
import { SucursalComponent } from './paginator/sucursal.component';
import { AdminComponent } from './paginator/admin.component';
import {CategoriaComponent} from './paginator/categoria.component';
import {ComisionComponent} from './paginator/comision.component';
import {ProductosucursalComponent} from './paginator/productosucursal.component';
import { CartSummaryComponent } from './client/cart-summary.component';
import { CartDetailComponent } from './client/cart-detail.component';
import { CheckoutComponent } from './client/checkout.component';

import { StoreModule } from './client/store.module';
import { OrdenesComponent } from './administrator/ordenes.component';
import { OrdenesbussinesComponent } from './bussines/ordenesbussines.component';
import {OrdenesclientComponent} from './client/ordenesclient.component';
import { RegisterComponent } from './login/register.component';
import { ListconsultasComponent } from './load-data-base/listconsultas.component';
import { FormconsultaComponent } from './load-data-base/formconsulta.component';
import { OrdenesadminComponent } from './paginator/ordenesadmin.component';
import { OrdenesclienteComponent } from './paginator/ordenescliente.component';
import { ProducoclienteComponent } from './paginator/producocliente.component';
import { AdmininicioComponent } from './administrator/admininicio.component';

registerLocaleData(localeES,'es');

const routes: Routes=[
  {path:'',redirectTo:'/login',pathMatch:'full'},//hace una redireccion a otra URL
 
  {path:'administrador',component:AdmininicioComponent},
  {path:'administrador/list_admin_sucursales',component:ListadminsucursalComponent},
  {path:'administrador/list_admin_sucursales/page/:page',component:ListadminsucursalComponent},
  {path:'administrador/form_admin_sucursal',component:FormadminsucursalComponent},
  {path:'administrador/admin_especifico/:id',component:FormadminsucursalComponent},
  
  {path:'administrador/list_negocios',component:AdministratorComponent},
  {path:'administrador/list_negocios/page/:page',component:AdministratorComponent},
  {path:'administrador/form_negocio',component:FormnegocioadminComponent},
  {path:'administrador/negocio_especifico/:id',component:FormnegocioadminComponent},

  {path:'administrador/list_comisiones',component:ListcomisionadminComponent},
  {path:'administrador/list_comisiones/page/:page',component:ListcomisionadminComponent},
  {path:'administrador/form_comision',component:FormcomisionadminComponent},
  {path:'administrador/comision_especifica/:id',component:FormcomisionadminComponent},

  {path:'administrador/list_ordenes',component:OrdenesComponent},
  {path:'administrador/list_ordenes/page/:page',component:OrdenesComponent},

  {path:'bussines',component:BussinesComponent},
  {path:'bussines/list_products',component:ListproductsbussinesComponent},
  {path:'bussines/list_productos/page/:page',component:ListproductsbussinesComponent},
  {path:'bussines/form_product',component:FormproductcomponentComponent},
  {path:'bussines/producto_especifico/:id',component:FormproductcomponentComponent},

  {path:'bussines/list_categorias',component:ListcategoriasComponent},
  {path:'bussines/list_categorias/page/:page',component:ListcategoriasComponent},
  {path:'bussines/form_categoria',component:FormcategoriaComponent},
  {path:'bussines/categoria_especifica/:id',component:FormcategoriaComponent},

  {path:'bussines/list_ordenes',component:OrdenesbussinesComponent},

  {path:'cart',component:CartDetailComponent},
  {path:'checkout',component:CheckoutComponent},
  
  {path:'client',component:ClientComponent},
  {path:'client/list_products/:id',component:ListproductsComponent},
  {path:'client/list_ordenes',component:OrdenesclientComponent},
  {path:'client/list_ordenes/page/:page',component:OrdenesclientComponent},
  
  {path:'database/load_database',component:LoadDataBaseComponent},
  {path:'database/list_conexiones',component:ListconexionesComponent},
  {path:'database/list_conexiones/page/:page',component:ListconexionesComponent},
  {path:'database/conexion_db',component:FormconnectionComponent},
  {path:'database/select_column',component:SelecolumnComponent},
  {path:'database/load_excel',component:LoadexcelComponent},
  {path:'database/relation_columns',component:RelationcolumnComponent},
  {path:'database/conexion_especifica/:id',component:FormconnectionComponent},
  {path:'database/list_consultas/:id',component:ListconsultasComponent},
  {path:'database/consulta_especifica/:id',component:FormconsultaComponent},

  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoadDataBaseComponent,
    ClientComponent,
    AdministratorComponent,
    BussinesComponent,
    HeaderComponent,
    FooterComponent,
    FormconnectionComponent,
    SelecolumnComponent,
    ListconexionesComponent,
    LoadexcelComponent,
    ListproductsComponent,
    ListproductsbussinesComponent,
    FilterPipe,
    RelationcolumnComponent,
    FormproductcomponentComponent,
    FormnegocioadminComponent,
    FormcomisionadminComponent,
    ListcomisionadminComponent,
    ListcategoriasComponent,
    FormcategoriaComponent,
    FormadminsucursalComponent,
    ListadminsucursalComponent,
    PaginatorComponent,
    SucursalComponent,
    AdminComponent,
    CategoriaComponent,
    ComisionComponent,
    ProductosucursalComponent,
    CartSummaryComponent,
    CartDetailComponent,
    CheckoutComponent,
    OrdenesComponent,
    OrdenesbussinesComponent,
    OrdenesclientComponent,
    RegisterComponent,
    ListconsultasComponent,
    FormconsultaComponent,
    OrdenesadminComponent,
    OrdenesclienteComponent,
    ProducoclienteComponent,
    AdmininicioComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    Select2Module,
    DataTablesModule,
    StoreModule,
    HotTableModule 
  ],
  providers: [LoaddbService,{provide:LOCALE_ID,useValue:'es'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
