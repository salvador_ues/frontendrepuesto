import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of, ObservedValueOf,throwError} from 'rxjs';
import {Loadb} from './loadb';
import {Conexion} from './conexion';
import { Select2OptionData } from 'ng2-select2';
import {TablasColumnas,ColumnasDbCentral,Columnas,Parametros,Tablas,ParametrosExcel,Consultas} from './tablas';
import {map,catchError,tap} from 'rxjs/operators';
import {formatDate,DatePipe} from '@angular/common';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {AuthService} from '../login/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoaddbService {
  private url:string ="http://localhost:8000/api_db/parametros_db";
  private url2:string ="http://localhost:8000/api_db/tablas_seleccion";
  private url3:string ="http://localhost:8000/api_db/tablas_columnas";
  private url4:string ="http://localhost:8000/api_db/list_conexiones";
  private url5:string ="http://localhost:8000/api_db/cargar_datos/save";
  private url6:string ="http://localhost:8000/api_db/conexion_especifica";
  private url7:string ="http://localhost:8000/api_db/conexion_establecer";
  private url8:string ="http://localhost:8000/api_db/conexion_modificar";
  private url9:string ="http://localhost:8000/api_db/conexion_eliminar";
  private url10:string ="http://localhost:8000/api_db/cargar_datos_excel/save";
  private url11:string = "http://localhost:8000/api_db/list_consulta";
  private url12:string = "http://localhost:8000/api_db/consulta";
  private url13:string = "http://localhost:8000/api_db/consulta/ejecutar";


  private tablas: Array<string>;
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});
  constructor(private http: HttpClient,private router:Router,private authService:AuthService) { }

  private agregarAuthorizationHeader(){
    let token = this.authService.token;
    if(token!=null){
      return this.httpHeaders.append('Authorization','Bearer '+token);
    }
    return this.httpHeaders;
  }

  private noAutorizado(e):boolean{
    if(e.status==401){
      if(this.authService.esAutenticado()){
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if(e.status==403){
      var ruta='';
      if(this.authService.usuario.roles.includes('ROLE_ADMIN')){
        ruta = '/administrador';
      }
      if(this.authService.usuario.roles.includes('ROLE_NEGOCIO')){
        ruta = '/bussines';
      }
      if(this.authService.usuario.roles.includes('ROLE_USER')){
        ruta = '/client';
      }
      Swal.fire('Acceso denegado','No tiene acceso','warning');
      this.router.navigate([ruta]);;
    }
    return false;
  }
 
  getMetadata():Observable<Loadb[]>{
    return this.http.get<Loadb[]>(this.url,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      })
    );
  }

  getDynamicList():Observable<Array<Select2OptionData>>{
    return this.http.get<Array<Select2OptionData>>(this.url,{headers:this.agregarAuthorizationHeader()});
  }

  crearConexion(conexion:Conexion): Observable<Conexion>{
    return this.http.post<Conexion>(this.url,conexion,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al crear conexion',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  enviarTabla(tablas:Tablas):Observable<Tablas>{
    return this.http.post<Tablas>(this.url2,tablas,{headers:this.agregarAuthorizationHeader()})
  }

  getTableColumn():Observable<string[]>{
    return this.http.get<string[]>(this.url3,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      })
    );
  }

  getListConexion( page:number):Observable<any>{
   return this.http.get(this.url4+ '/page/'+page,{headers:this.agregarAuthorizationHeader()}).pipe(
    //  tap(response=>{
    //    let conexiones = response as Conexion[];
    //    conexiones.forEach(conexion=>{
    //      console.log(conexion.usuario);
    //    })
    //  }), 
    catchError(e=>{
      if(this.noAutorizado(e)){
        return throwError(e);
      }
      if(e.status==400){
        return throwError(e);
      }
      console.log(e.error.respuesta);
      return throwError(e);
    }),
     map( (response:any)=>{
        (response.content as Conexion[]).map(conexion=>{
        conexion.nombre = conexion.nombre;
        conexion.puerto = conexion.puerto;
        conexion.gestor = conexion.gestor;
        conexion.usuario = conexion.usuario;
       // let datePipe = new DatePipe('es');
        return conexion;
       }); 
       return response;
      }
      ),
      // tap(response=>{
      //   response.forEach(conexion=>{
      //     console.log(conexion.usuario);
      //   })
      // })
   );
  }

  enviarParametros(parametros:Parametros){
    return this.http.post<Parametros>(this.url5,parametros,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        //this.router.navigate(['/database/list_conexiones']);
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al guardar los registros, verifique si esta insertando tipo de dato correcto en columna',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  getConexionEspecifica(id):Observable<Conexion>{
    return this.http.get<Conexion>(`${this.url6}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        this.router.navigate(['/database/list_conexiones']);
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al obtener conexion',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  conexion(conexion:Conexion):Observable<Conexion>{
    return this.http.put<Conexion>(`${this.url7}/${conexion.conexion_id}`,conexion,{headers:this.agregarAuthorizationHeader()});
  }

  conexionModificar(conexion:Conexion):Observable<Conexion>{
    return this.http.put<Conexion>(`${this.url8}/${conexion.conexion_id}`,conexion,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al actualizar conexion',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  conexionEliminar(id:string):Observable<Conexion>{
    return this.http.delete<Conexion>(`${this.url9}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al eliminar conexion',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  cargarDatosCSV(parametros:ParametrosExcel){
    return this.http.post<ParametrosExcel>(this.url10,parametros,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al cargar datos CSV',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    )
  }
//GESTION DE CONSULTAS
  getListConsultas(id):Observable<Consultas[]>{
    return this.http.get(`${this.url11}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        return throwError(e);
      })
      ,map((response)=>response as Consultas[])
    );
  }
  
  consultaModificar(consulta:Consultas):Observable<Consultas>{
    return this.http.put<Consultas>(`${this.url12}/${consulta.consulta_id}`,consulta,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        console.log(e.error.respuesta);
        Swal.fire({
          icon:'error',
          title:'Error al editar consulta',
          text:e.error.respuesta+' '+'error'
        });
        return throwError(e);
      })
    );
  }

  getConsulta(id):Observable<Consultas>{
    return this.http.get<Consultas>(`${this.url12}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        this.router.navigate(['/database/list_conexiones']);
        Swal.fire({
          icon:'error',
          title:'Error al obtener consulta',
          text:e.error.respuesta+' '+'error',
        });
        return throwError(e);
      })
    );
  }

  consultaEjecutar(id):Observable<Consultas>{
    return this.http.get<Consultas>(`${this.url13}/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{
        if(this.noAutorizado(e)){
          return throwError(e);
        }
        if(e.status==400){
          return throwError(e);
        }
        this.router.navigate(['/database/list_conexiones']);
        Swal.fire({
          icon:'error',
          title:'Error al ejecutar consulta',
          text:e.error.respuesta+' '+'error',
        });
        return throwError(e);
      })
    );
  }
//TERMINA CONSULTAS
}
