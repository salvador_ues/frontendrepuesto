import { Component, OnInit } from "@angular/core";
import { LoaddbService } from "./loaddb.service";
import { Conexion } from "./conexion";
import { Router, ActivatedRoute } from "@angular/router";
import swal from "sweetalert2";

@Component({
  selector: "app-listconexiones",
  templateUrl: "./listconexiones.component.html",
  styleUrls: ["./listconexiones.component.css"]
})
export class ListconexionesComponent implements OnInit {
  conexion: Conexion[];
  paginador: any;
  constructor(
    private loadService: LoaddbService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params => {
      let page: number = +params.get("page");
      if (!page) {
        page = 0;
      } 
      this.loadService.getListConexion(page).subscribe(response => {
        this.conexion = response.content as Conexion[];
        this.paginador = response;
      });
    });
  } 

  conexionEliminar(conexion: Conexion): void {
    swal
      .fire({
        title: "Estás seguro?",
        text: "Una ves eliminado no se podra deshacer la operacion!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, elimina este registrot!"
      })
      .then(result => {
        if (result.value) {
          this.loadService
            .conexionEliminar(conexion.conexion_id)
            .subscribe(response => {
              this.conexion = this.conexion.filter(con => con !== conexion);
              swal.fire(
                "Eliminado!",
                "Tu registro ha sido eliminado.",
                "success"
              );
            });
        }
      });
  }
}
