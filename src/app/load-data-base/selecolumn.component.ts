import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  TablasColumnas,
  Columnas,
  ColumnasDbCentral,
  Parametros
} from "./tablas";
import { LoaddbService } from "./loaddb.service";
import Swal from "sweetalert2";
import * as jquery from 'jquery';
@Component({
  selector: "app-selecolumn",
  templateUrl: "./selecolumn.component.html",
  styleUrls: ["./selecolumn.component.css"]
})
export class SelecolumnComponent implements OnInit {
  private relationColumns: ColumnasDbCentral = new ColumnasDbCentral();
  //private parametros:string[];
  private parametros: Parametros = new Parametros();
  //tablaColumna:TablasColumnas[];
  tablaColumna: string[];
  //private columnas: Columnas = new Columnas();
  columnas: Columnas[];
  public columnaselect: Columnas[];
  public;
  columnasociacion: Columnas = new Columnas();
  prueba: string;
  prueba2: Array<string>;
  public indice = [];

  //displayTablas: string[]=['PRODUCTO nombre','PRODUCTO descripcion','PRODUCTO precio','PRODUCTO cantidad','PRODUCTO subcategoria','SUBCATEGORIA nombre'];
  constructor(private loadbService: LoaddbService, private router: Router) {}

  ngOnInit() {
    this.loadbService
      .getTableColumn()
      .subscribe(tablaColumna => (this.tablaColumna = tablaColumna));

      jquery('.select-columns-db').select2();
  }

  submit(): void {
    console.log("aqui");
    console.log(this.columnas);
    this.columnaselect = this.columnas;

    for (let index = 0; index < this.columnaselect.length; index++) {
      this.indice[index] = index + 1;
    }
  }

  enviarParametros(): void {
    console.log("HAY VALOR:" + this.columnaselect);
    // console.log("AQUI HAY:"+this.parametros.columasSeleccionadas);
    if (
      this.relationColumns.nombre === this.relationColumns.descripcion ||
      this.relationColumns.nombre === this.relationColumns.precio ||
      this.relationColumns.nombre === this.relationColumns.cantidad ||
      this.relationColumns.nombre === this.relationColumns.subcategoria ||
      this.relationColumns.descripcion === this.relationColumns.precio ||
      this.relationColumns.descripcion === this.relationColumns.cantidad ||
      this.relationColumns.descripcion === this.relationColumns.subcategoria ||
      this.relationColumns.precio === this.relationColumns.cantidad ||
      this.relationColumns.precio === this.relationColumns.subcategoria ||
      this.relationColumns.cantidad === this.relationColumns.subcategoria
    ) {
      Swal.fire(
        "Error relación",
        "Numero relación repetido, modifique dato",
        "error"
      );
    } else {
      this.parametros.columasSeleccionadas = this.columnaselect;
      this.parametros.columnas = this.relationColumns;
      //this.parametros.columasSeleccionadas=this.columnaselect;
      console.log(
        "TODOS LOS PARAMETROS:" +
          this.parametros.columasSeleccionadas +
          " " +
          this.parametros.columnas.nombre
      );

      this.loadbService
        .enviarParametros(this.parametros)
        .subscribe(response =>{
          this.router.navigate(["/bussines/list_products"])
          Swal.fire('Datos cargados','Registros creados con exito','success') 
        }
        );
    }
  }
}
