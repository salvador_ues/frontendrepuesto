import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { LoaddbService } from "./loaddb.service";
import { Consultas } from './tablas';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-formconsulta',
  templateUrl: './formconsulta.component.html',
  styleUrls: ['./formconsulta.component.css']
})
export class FormconsultaComponent implements OnInit {
  private consulta:Consultas = new Consultas();
  constructor(private router: Router,private activateRoute:ActivatedRoute,private loadService: LoaddbService) { }

  ngOnInit() {
    this.getConsulta();
  }

  public getConsulta():void{
    this.activateRoute.params.subscribe(params=>{
      let id=params['id'];
      if(id){
        this.loadService.getConsulta(id).subscribe(
          (consulta)=>this.consulta=consulta
        )
      }
    })
  }

  public consultaEjecutar():void{
    this.loadService.consultaEjecutar(this.consulta.consulta_id).subscribe(
      response=>{this.router.navigate(['/bussines/list_products'])
      Swal.fire('Consulta','Extracción de datos exitoso','success') 
    }
    )
  }

  public consultaModificar():void{
    this.loadService.consultaModificar(this.consulta).subscribe(
      response=>{this.router.navigate(['/database/list_consultas', this.consulta.conexion.conexion_id])
      Swal.fire('Consulta','Consulta editada exitosamente','success') 
    }
    )
  }

}
