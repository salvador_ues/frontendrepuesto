import { Component, OnInit } from '@angular/core';
import {Conexion} from './conexion';
import {LoaddbService} from './loaddb.service';
import {Router,ActivatedRoute} from '@angular/router';
import { ParsedVariable } from '@angular/compiler';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-formconnection',
  templateUrl: './formconnection.component.html',
  styleUrls: ['./formconnection.component.css']
})
export class FormconnectionComponent implements OnInit {
  private conexion: Conexion= new Conexion();
  manager: any=[
    {id:'postgresql',name:"Postgres"},
    {id:'mysql',name:"MySQL"},
    {id:'sqlserver',name:"SqlServer"}
  ];

  constructor(private loadbService: LoaddbService, private router: Router,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
     this.getConexionEspecifica();
  }

  public getConexionEspecifica():void{
    this.activatedRoute.params.subscribe(params=>{
      let id = params['id']
      if(id){
        this.loadbService.getConexionEspecifica(id).subscribe(
          (conexion)=>this.conexion=conexion
        )
      }
    })
    console.log(this.conexion);
  }

  public conexionEstablecer():void{
    this.loadbService.conexion(this.conexion).subscribe(
      response=>{this.router.navigate(['/database/load_database'])
      Swal.fire('Conexión','Conexión establecida creado con exito','success') 
    }
    )
  }

  public conexionModificar():void{
    this.loadbService.conexionModificar(this.conexion).subscribe(
      response=> {this.router.navigate(['/database/list_conexiones'])
      Swal.fire('Registro editado','Registro editado con exito','success') 
    }
    )
  }

  public create():void{
     this.loadbService.crearConexion(this.conexion).subscribe(
        response=> {this.router.navigate(['/database/load_database'])
        Swal.fire('Nuevo registro','Registro creado con exito','success') 
      }
      )
  }


}
