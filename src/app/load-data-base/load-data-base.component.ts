import { Component, OnInit } from '@angular/core';
import {Loadb} from './loadb';
import {Tablas} from './tablas';
import {LoaddbService} from './loaddb.service';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import {Observable,of} from 'rxjs';
import { Select2OptionData  } from 'ng2-select2';  
import Swal from 'sweetalert2';
import * as jquery from 'jquery';

//import {Router} from '@angular/router';
import {Router, ActivatedRoute} from '@angular/router';
@Component({
  selector: 'load-data-base',
  templateUrl: './load-data-base.component.html',
  styleUrls: ['./load-data-base.component.css']
})
export class LoadDataBaseComponent implements OnInit {

  private tablas: Tablas = new Tablas();
  //tablas:string;
  //clase
  servicio: LoaddbService;
  form: FormGroup;
  loadb:Loadb[];
 // public exampleData: Array<Select2OptionData >;
  prueba: Array<string>;
  displayTablas: string[]=['producto','subcategoria','categoria'];
  
  constructor(private loadbService: LoaddbService, private formBuilder: FormBuilder,private router: Router,private activatedRoute:ActivatedRoute) {

   }

  ngOnInit() {
    this.loadbService.getMetadata().subscribe(
      //funcion anonima simplificada, lo primero es el argumento que se pasa a la funcion 
      //y se asigna a loadb variable de la funcion anonima
      loadb=>this.loadb = loadb
      
    );

    jquery('.select-columns-db').select2();
  }

  
  public enviarTabla():void {
    this.loadbService.enviarTabla(this.tablas).subscribe(
      response=> {this.router.navigate(['/database/select_column'])
      Swal.fire('Columnas seleccionadas','Columnas seleccionadas con exito','success') 
    }
    );
    console.log(this.tablas);
  }

}
