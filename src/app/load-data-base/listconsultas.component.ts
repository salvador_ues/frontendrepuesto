import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { LoaddbService } from "./loaddb.service";
import { Consultas } from './tablas';

@Component({
  selector: 'app-listconsultas',
  templateUrl: './listconsultas.component.html',
  styleUrls: ['./listconsultas.component.css']
})
export class ListconsultasComponent implements OnInit {
  consultas: Consultas[];
  constructor(private router: Router,private activateRoute:ActivatedRoute,private loadService: LoaddbService) { }

  ngOnInit() {
    this.loadConsultas();
    console.log("AQUI CONSULTAS:"+this.consultas);
  } 

  loadConsultas():void{
    this.activateRoute.params.subscribe(params=>{
      let id = params['id']
      console.log("ID conexion:"+id);
        this.loadService.getListConsultas(id).subscribe(
          consulta=>{this.consultas=consulta}
        
        );
    });
    console.log(this.consultas);
  }

}
