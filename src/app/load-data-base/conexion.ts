export class Conexion{
    conexion_id: string;
    gestor:string;
    nombre: string;
    host: string;
    puerto: string;
    usuario: string;
    password: string;
}