import { Component, OnInit } from "@angular/core";
import * as XLSX from "xlsx";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import {
  TablasColumnas,
  Columnas,
  ColumnasDbCentral,
  Parametros,
  ParametrosExcel
} from "./tablas";
import { LoaddbService } from "./loaddb.service";

@Component({
  selector: "app-loadexcel",
  templateUrl: "./loadexcel.component.html",
  styleUrls: ["./loadexcel.component.css"]
})
export class LoadexcelComponent implements OnInit {
  private relationColumns: ColumnasDbCentral = new ColumnasDbCentral();
  public dataset:any[];
  arrayBuffer: any;
  file: File;
  public ids2 = [];
  public prueba: string;
  public ids3 = [];
  public ids4: any[];
  public columnasSelect: Columnas[];
  public columnasList: Columnas[];
  show: boolean = false;
  public indice = [];
  private parametrosExcel: ParametrosExcel = new ParametrosExcel();

  constructor(private loadbService: LoaddbService, private router: Router) {}

  ngOnInit() {}

  fileUpload(files) {
    if (files && files.length > 0) {
      const file: File = files.item(0);
      const reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = e => {
        const res = reader.result as string; // This variable contains your file as text
        const lines = res.split("\n"); // Splits you file into lines
        this.dataset=lines;
        const ids = [];
        const idsprueba = [];
        var contador = 0;
        lines.forEach(line => {
          ids.push(line.split(",")[0]); // Get first item of line
        });
        ids.forEach(id => {
          this.ids2.push(id.split(";"));
          if (contador < 1) {
            this.ids3.push(id.split(";"));
          }
          contador++;
        });
        if (this.ids3.length > 0) {
          this.show = true;
        } else {
          this.show = false;
        }
      };
    }
  }

  columnasSeleccionadas(): void {
    console.log(this.columnasSelect);
    this.columnasList = this.columnasSelect;
    console.log("columnasList");
    console.log(this.columnasList);

    for (let index = 0; index < this.columnasList.length; index++) {
      this.indice[index] = index + 1;
    }
    console.log(this.indice);
  }

  enviarParametros(): void {
    console.log(this.columnasList);
    console.log(this.relationColumns);
    console.log(this.ids2);
    this.parametrosExcel.columnas = this.relationColumns;
    if (
      this.relationColumns.nombre === this.relationColumns.descripcion ||
      this.relationColumns.nombre === this.relationColumns.precio ||
      this.relationColumns.nombre === this.relationColumns.cantidad ||
      this.relationColumns.nombre === this.relationColumns.subcategoria ||
      this.relationColumns.descripcion === this.relationColumns.precio ||
      this.relationColumns.descripcion === this.relationColumns.cantidad ||
      this.relationColumns.descripcion === this.relationColumns.subcategoria ||
      this.relationColumns.precio === this.relationColumns.cantidad ||
      this.relationColumns.precio === this.relationColumns.subcategoria ||
      this.relationColumns.cantidad === this.relationColumns.subcategoria
    ) {
      Swal.fire(
        "Error relación",
        "Numero relación repetido, modifique dato",
        "error"
      );
    } else {
      this.parametrosExcel.columasSeleccionadas = this.columnasList;
      this.parametrosExcel.datosExcel = this.ids2;
      console.log(this.parametrosExcel.datosExcel);
      this.loadbService
        .cargarDatosCSV(this.parametrosExcel)
        .subscribe(response =>{
          this.router.navigate(["/bussines/list_products"])
          Swal.fire('CSV cargado','Registros creados con exito','success') 
        }
        );
    }
  }
}
