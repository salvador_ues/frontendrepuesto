import{ Conexion } from './conexion';

export class Consultas{
    consulta_id:number;
    consulta:string;
    conexion:Conexion;
    tipoconsulta:string;
}

export class Tablas {
    tabla: string;

}

export class TablasColumnas{
    tabla:string;
    columna:string;
}

export class Columnas{
    columna:string;
}

export class ColumnasDbCentral{
    nombre:number;
    descripcion:number;
    precio:number;
    cantidad:number;
    subcategoria:number;
    subcategoria_nombre:number;

}

export class Parametros{
    columnas:ColumnasDbCentral;
    columasSeleccionadas:Columnas[];

    
    public get columna() : ColumnasDbCentral {
        return this.columnas;
    }

    
    public set columna(columnas : ColumnasDbCentral) {
        this.columnas;
    }

    public get columnaSeleccionada():Columnas[]{
        return this.columasSeleccionadas;
    }

    public set columnaSeleccionada(columasSeleccionadas:Columnas[]){
        this.columasSeleccionadas;
    }
}

export class ParametrosExcel{
    columnas:ColumnasDbCentral;
    columasSeleccionadas:Columnas[];
    datosExcel:string[];

    public get columna() : ColumnasDbCentral {
        return this.columnas;
    }

    
    public set columna(columnas : ColumnasDbCentral) {
        this.columnas;
    }

    public get columnaSeleccionada():Columnas[]{
        return this.columasSeleccionadas;
    }

    public set columnaSeleccionada(columasSeleccionadas:Columnas[]){
        this.columasSeleccionadas;
    }

    public get datoExcel():string[]{
        return this.datosExcel;
    }

    public set datoExcel(datosExcel:string[]){
        this.datosExcel;
    }
}