import { Component, OnInit } from '@angular/core';
import{ Usuario } from './usuario';
import Swal from 'sweetalert2';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usuario:Usuario;
  titulo:string = "Por favor inicie sesión";
  constructor(private authService:AuthService,private router:Router) {
    this.usuario = new Usuario();
   }

  ngOnInit() {
    if(this.authService.esAutenticado()){
      Swal.fire('Login','Ya ha iniciado sesión','info');
      this.router.navigate(['/bussines']);
    }
  }

  login():void{
    console.log(this.usuario);
    if(this.usuario.username==null||this.usuario.password==null){
      Swal.fire('Error Login','Username o password vacio','error');
      return;
    }
      this.authService.login(this.usuario).subscribe(response=>{
        var ruta='';
        console.log(response);
        this.authService.guardarUsuario(response.access_token);
        this.authService.guardarToken(response.access_token);
        if(this.authService.usuario.roles.includes('ROLE_ADMIN')){
          ruta = '/administrador';
        }
        if(this.authService.usuario.roles.includes('ROLE_NEGOCIO')){
          ruta = '/bussines';
        }
        if(this.authService.usuario.roles.includes('ROLE_USER')){
          ruta = '/client';
        }
        this.router.navigate([ruta])
        Swal.fire(
          'Login',
          'Inicio de sesión con exito',
          'success' 
        )
      },error=>{
        if(error.status==400){
          Swal.fire(
            'Error login',
            'Error de sesión o calves incorrectas',
            'error' 
          )
        }
      }
      );
  }
}
