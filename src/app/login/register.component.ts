import { Component, OnInit } from '@angular/core';
import {Usuario} from './usuario';
import {Router,ActivatedRoute} from '@angular/router';
import {AuthService} from './auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public usuario:Usuario = new Usuario();
  public titulo:string = "Registrar usuario";
  public password2:string;
  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

  register():void{
    if(this.usuario.password.localeCompare(this.password2)!=0){
      Swal.fire('Error password','Claves no coinciden','error');
    }else{
      this.authService.register(this.usuario).subscribe(
        response => {this.router.navigate(['/login'])
        Swal.fire('Registro','Registro con exito','success')
      } 
      );
    }
    
  }

}
