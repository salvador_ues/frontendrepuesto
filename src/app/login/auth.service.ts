import { Injectable } from '@angular/core';
import { Observable,throwError } from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Usuario} from './usuario';
import {map,catchError,tap} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _usuario:Usuario;
  private _token:string;
  private httpHeaders2 = new HttpHeaders({'Content-Type':'application/json'});
  constructor(private http:HttpClient) { }

  login(usuario:Usuario):Observable<any>{
    const url= 'http://localhost:8000/oauth/token';
    const credenciales = btoa('angularapp'+':'+'12345');
    const httpHeaders = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded','Authorization':'Basic '+credenciales});
    let params = new URLSearchParams();
    params.set('grant_type','password');
    params.set('username',usuario.username);
    params.set('password',usuario.password);
    return this.http.post<any>(url,params.toString(),{headers:httpHeaders});
  }

  guardarUsuario(accessToken:string):void{
    let payload=this.obtenerDatosToken(accessToken);
    this._usuario = new Usuario();
    this._usuario.nombre = payload.nombre_usuario;
    // this._usuario.apellido = payload.apellido;
    this._usuario.email = payload.email;
    this._usuario.username = payload.user_name;
    this._usuario.roles = payload.authorities;
    sessionStorage.setItem('usuario',JSON.stringify(this._usuario));
    
  }

  guardarToken(accessToken:string):void{
    this._token = accessToken;
    sessionStorage.setItem('token',this._token);
  }

  obtenerDatosToken(accessToken:string):any{
    if(accessToken!=null){
      return JSON.parse(atob(accessToken.split(".")[1]));
    }
    return null;
  }

  esAutenticado():boolean{
    let payload = this.obtenerDatosToken(this.token);
    if(payload!=null && payload.user_name && payload.user_name.length>0){
      return true;
    }
    return false;
  }

  public get token():string{
    if(this._token!=null){
      return this._token;
    }else if(this._token == null&& sessionStorage.getItem('token')!=null){
      this._token = sessionStorage.getItem('token');
      return this._token;
    }
    return null;  
  }

  public get usuario():Usuario{
    if(this._usuario!=null){
      return this._usuario;
    }else if(this._usuario==null && sessionStorage.getItem('usuario')!=null){
      this._usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
      return this._usuario;
    }
    return new Usuario();
  }

  // public get usuario():Usuario{

  // }

  logout():void{
    this._usuario=null;
    this._token=null;
    sessionStorage.clear();
    //por si queremos eliminar item ciertos
    //sessionStorage.removeItem("token");
  }

  hasRole(role:string):boolean{
    if(this.usuario.roles.includes(role)){
      return true;
    }
    return false;
  }

  private agregarAuthorizationHeader(){
    let token = this.token;
    if(token!=null){
      return this.httpHeaders2.append('Authorization','Bearer '+token);
    }
    return this.httpHeaders2;
  }


    register(usuario:Usuario):Observable<any>{
      const url="http://localhost:8000/api_registrar/registrar";
      return this.http.post<Usuario>(url,usuario,{headers:this.agregarAuthorizationHeader()}).pipe(
        catchError(e=>{
          if(e.status==400){
            return throwError(e);
          }
          console.log(e.error.respuesta);
          Swal.fire({
            icon:'error',
            title:'Error al crear registro',
            text:e.error.respuesta+' '+'error'
          });
          return throwError(e);
        })
      );
    }
}
